package net.sssubtlety.wither_cage_fix.mixin_helper;

import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public interface WitherEntityMixinAccessor {
    @Nullable UUID wither_cage_fix$getSavedUuid();
    void wither_cage_fix$clearSavedUuid();
    void wither_cage_fix$setCurrentUuid(UUID targetUuid);
}
