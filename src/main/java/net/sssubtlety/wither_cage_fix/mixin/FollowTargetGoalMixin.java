package net.sssubtlety.wither_cage_fix.mixin;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.ai.goal.TargetGoal;
import net.minecraft.entity.ai.goal.TrackTargetGoal;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.server.world.ServerWorld;
import net.sssubtlety.wither_cage_fix.mixin.accessor.TrackTargetGoalAccessor;
import net.sssubtlety.wither_cage_fix.mixin_helper.WitherEntityMixinAccessor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.UUID;

import static net.sssubtlety.wither_cage_fix.mixin.accessor.WitherEntityAccessor.wither_cage_fix$getHEAD_TARGET_PREDICATE;

@Mixin(TargetGoal.class)
abstract class FollowTargetGoalMixin<T extends LivingEntity> extends TrackTargetGoal {
    @Nullable @Shadow protected LivingEntity targetEntity;

    @Unique private static final TargetPredicate WITHER_TARGET_PREDICATE_IGNORE_VISIBILITY =
        wither_cage_fix$getHEAD_TARGET_PREDICATE().copy().ignoreVisibility();

    private FollowTargetGoalMixin() {
        super(null, false);
        throw new IllegalStateException("FollowTargetGoal's dummy constructor called!");
    }

    @Inject(method = "findClosestTarget", at = @At(value = "TAIL"))
    private void checkMobTargetFirst(CallbackInfo ci) {
        if (!(((TrackTargetGoalAccessor)this).wither_cage_fix$getMob() instanceof WitherEntity witherEntity)) {
            return;
        }

        if (!(witherEntity.getWorld() instanceof ServerWorld world)) {
            return;
        }

        final WitherEntityMixinAccessor witherEntityMixin = (WitherEntityMixinAccessor) witherEntity;
        final UUID savedTargetUuid = witherEntityMixin.wither_cage_fix$getSavedUuid();
        if (savedTargetUuid != null) {
            if (
                (world.getEntity(savedTargetUuid) instanceof LivingEntity livingEntity) &&
                WITHER_TARGET_PREDICATE_IGNORE_VISIBILITY.test(world, witherEntity, livingEntity)
            ) {
                // re-assign if wither remembers a target
                this.targetEntity = livingEntity;
                witherEntityMixin.wither_cage_fix$setCurrentUuid(savedTargetUuid);
                witherEntityMixin.wither_cage_fix$clearSavedUuid();
                return;
            }
        }

        witherEntityMixin.wither_cage_fix$setCurrentUuid(
            this.targetEntity == null ? null : this.targetEntity.getUuid()
        );
    }
}
