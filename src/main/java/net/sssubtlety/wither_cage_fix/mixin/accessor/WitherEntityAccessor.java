package net.sssubtlety.wither_cage_fix.mixin.accessor;

import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.boss.WitherEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(WitherEntity.class)
public interface WitherEntityAccessor {
    @Accessor("HEAD_TARGET_PREDICATE")
    static TargetPredicate wither_cage_fix$getHEAD_TARGET_PREDICATE() {
        throw new UnsupportedOperationException();
    }
}
