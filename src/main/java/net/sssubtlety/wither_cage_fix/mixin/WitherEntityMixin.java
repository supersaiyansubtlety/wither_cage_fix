package net.sssubtlety.wither_cage_fix.mixin;

import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.nbt.NbtCompound;
import net.sssubtlety.wither_cage_fix.mixin_helper.WitherEntityMixinAccessor;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.UUID;

import static net.sssubtlety.wither_cage_fix.WitherCageFix.NAMESPACE;

@Mixin(WitherEntity.class)
abstract class WitherEntityMixin extends HostileEntity implements WitherEntityMixinAccessor {
	@Unique private static final String HAS_MAIN_TARGET_OLD = "has_main_target";
	@Unique private static final String HAS_MAIN_TARGET = NAMESPACE + ":HasMainTarget";
	@Unique private static final String MAIN_TARGET_UUID_OLD = "main_target_uiid";
	@Unique private static final String MAIN_TARGET_UUID = NAMESPACE + ":MainTargetUuid";

	@Unique @Nullable private UUID savedTargetUuid;
	@Unique @Nullable private UUID currentTargetUuid;

	private WitherEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
		throw new IllegalStateException("WitherSkullEntityMixin's dummy constructor called!");
	}

	@Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
	private void writeTargetsToTag(NbtCompound nbt, CallbackInfo ci) {
		if (this.getWorld().isClient()) {
            return;
        }

		if (this.currentTargetUuid == null) {
            nbt.putBoolean(HAS_MAIN_TARGET, false);
        } else {
			nbt.putBoolean(HAS_MAIN_TARGET, true);
			nbt.putUuid(MAIN_TARGET_UUID, this.currentTargetUuid);
		}
	}

	@Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
	private void readTargetsFromTag(NbtCompound nbt, CallbackInfo ci) {
		if (this.getWorld().isClient()) {
            return;
        }
			
		if (nbt.getBoolean(HAS_MAIN_TARGET)) {
			this.savedTargetUuid = nbt.getUuid(MAIN_TARGET_UUID);
		} else if (nbt.getBoolean(HAS_MAIN_TARGET_OLD)) {
			this.savedTargetUuid = nbt.getUuid(MAIN_TARGET_UUID_OLD);
		}
	}

	@Override
	public UUID wither_cage_fix$getSavedUuid() {
		return this.savedTargetUuid;
	}

	@Override
	public void wither_cage_fix$clearSavedUuid() {
		this.savedTargetUuid = null;
	}

	@Override
	public void wither_cage_fix$setCurrentUuid(UUID targetUuid) {
        this.currentTargetUuid = targetUuid;
	}
}