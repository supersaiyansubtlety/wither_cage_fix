package net.sssubtlety.wither_cage_fix.mixin;

import net.minecraft.entity.projectile.ExplosiveProjectileEntity;
import net.minecraft.entity.projectile.WitherSkullEntity;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static net.sssubtlety.wither_cage_fix.WitherCageFix.areBlueWitherSkullsAllowed;

@Mixin(WitherSkullEntity.class)
abstract class WitherSkullEntityMixin extends ExplosiveProjectileEntity {
	private WitherSkullEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
		throw new IllegalStateException("WitherSkullEntityMixin's dummy constructor called!");
	}

	@Inject(method = "setCharged", at = @At("HEAD"), cancellable = true)
	private void dontSetCharged(CallbackInfo ci) {
		if (this.getWorld() instanceof ServerWorld serverWorld && !areBlueWitherSkullsAllowed(serverWorld)) {
            ci.cancel();
        }
	}
}
