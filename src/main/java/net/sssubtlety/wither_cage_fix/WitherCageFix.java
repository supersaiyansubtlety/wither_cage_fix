package net.sssubtlety.wither_cage_fix;

import net.minecraft.server.world.ServerWorld;

public class WitherCageFix {
	public static final String NAMESPACE = "wither_cage_fix";

	public static boolean areBlueWitherSkullsAllowed(ServerWorld world) {
		return world.getGameRules().get(Init.ALLOW_BLUE_WITHER_SKULLS).getValue();
	}
}
