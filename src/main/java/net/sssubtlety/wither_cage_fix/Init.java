package net.sssubtlety.wither_cage_fix;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.world.GameRules;

import static net.sssubtlety.wither_cage_fix.WitherCageFix.NAMESPACE;

public class Init implements ModInitializer {
    static final GameRules.Key<GameRules.BooleanGameRule> ALLOW_BLUE_WITHER_SKULLS = GameRuleRegistry.register(
        NAMESPACE + ":allowBlueWitherSkulls",
        GameRules.Category.MOBS,
        GameRuleFactory.createBooleanRule(false)
    );

    @Override
    public void onInitialize() { }
}
