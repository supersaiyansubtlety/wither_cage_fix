- 1.1.1 (17 Dec. 2024): Marked as compatible with 1.21.4
- 1.1.0 (1 Dec. 2024): Updated for 1.21.2-1.21.3
- 1.0.23 (28 Aug. 2024): Marked as compatible with 1.21.1
- 1.0.22 (14 Jun. 2024):
  - Marked as compatible with 1.21
  - Updated translations
- 1.0.21 (9 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
  - Minor internal changes
- 1.0.20 (25 Apr. 2024): Marked as compatible with 1.20.5
- 1.0.19 (1 Feb. 2024): Marked as compatible with 1.20.3 and 1.20.4
- 1.0.18 (1 Nov. 2023):
  - updated for 1.20.2
  - renamed gamerule `allowBlueWitherSkulls` to `wither_cage_fix:allowBlueWitherSkulls` and changed the default value to
  `false` so updating worlds won't have blue skulls
  - minor internal changes
- 1.0.17 (23 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.16 (22 Mar. 2023): Marked as compatible with 1.19.3 and 1.19.4
- 1.0.15 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.14 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.0.13 (23 Jun. 2022): Updated for 1.19!
- 1.0.12 (15 Mar. 2022): Updated for 1.18.2
- 1.0.11 (12 Dec. 2021): Marked as compatible with 1.18.1
- 1.0.10 (8 Dec. 2021): 

  Updated for 1.18!

  Reworked a mixin to improve theoretical compatibility.

- 1.0.9 (9 Jul. 2021): Marked as compatible with 1.17.1
- 1.0.8 (9 Jun. 2021): Updated for 1.17
- 1.0.7 (6 Jun. 2021): Updated for 1.17-rc1
- 1.0.6-1a (6 Jun. 2021): Dummy release testing cursegradle
- 1.0.6 (21 Apr.2021): Fixed mod name appearing as 'Mod Name' in Mod Menu. 
- 1.0.5 (11 Mar. 2021): Fixed `allowBlueWitherSkulls` on servers. 
- 1.0.4 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.0.3 (3 Nov. 2020): Marked as compatible with 1.16.4. 
- 1.0.2 (17 Sep. 2020): Marked as compatible with 1.16.3. Withers remembering their targets will no longer check for
line-of-sight (which was defeating the purpose of the mod, making it do basically nothing. Sorry about that.). 
- 1.0.1 (7 Sep. 2020): Added some additional checks to ensure the Wither's normal targeting rules are followed when
'remembering' their target entities. 
  Streamlined some code. Allowed use of newer Fabric API versions. 
- 1.0.0 (6 Sep. 2020): Initial release. 